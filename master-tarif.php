<?php
session_start();
// if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == '') {
//     echo "<script>alert('Anda belum login atau session anda habis, silakan login ulang.');
//                         window.location='/simrs/pendidikan_nondm/';
//                         </script>";
// }
include '../connect/konek.php';
include '../head_menu.php';
include 'header.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tarif</title>

    <!-- bootstrap 4 -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- datatables -->
    <link rel="stylesheet" href="assets/plugins/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css">
    <!-- sweetalert2 -->
    <link rel="stylesheet" href="assets/plugins/sweetalert2/sweetalert2.min.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/fontawesome.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/brands.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/solid.css">

</head>

<body>
    <!-- Content -->
    <div class="container bg-light" style="height: 100%;">
        <div class="text-center">
            <h5><b>.: Master Tarif :.</b></h5>
        </div>
        <div class="m-3">
            <button class="btn btn-sm btn-primary button-create">
                <i class="fas fa-plus-circle"></i>
                Tambah Tarif
            </button>
        </div>

        <!-- Table -->
        <div class="m-3">
            <table id="tabel_tarif" class="table table-sm table-bordered table-striped table-hover" style="width:100%;">
                <thead class="bg-light">
                    <tr>
                        <th>#</th>
                        <th>Jenis Penelitian</th>
                        <th>Jenjang Pendidikan</th>
                        <th>Biaya</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody id="body"></tbody>
            </table>
        </div>
        <!-- End Table -->
    </div>
    <!-- End Content -->

    <!-- Modal -->
    <div id="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Tarif</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form id="form">
                            <div class="form-group">
                                <label for="penelitian">Jenis penelitian</label>
                                <select name="penelitian" id="penelitian" class="form-control">
                                    <option value="" hidden>-- Pilih jenis penelitian --</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pendidikan">Jenis pendidikan</label>
                                <select name="pendidikan" id="pendidikan" class="form-control">
                                    <option value="" hidden>-- Pilih jenis pendidikan --</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama Tarif</label>
                                <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan nama tarif...">
                            </div>
                            <div class="form-group">
                                <label for="biaya">Biaya</label>
                                <input type="number" min="0" step="5000" name="biaya" id="biaya" class="form-control" value="0" />
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" name="btn-update" class="btn btn-sm btn-primary button-modal"><i class="fas fa-save"></i> Simpan</button>
                    <button type="button" class="btn btn-sm btn-danger btn-close" data-dismiss="modal"><i class="fas fa-redo"></i> Batal</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->



    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="assets/plugins/DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="master-tarif.js"></script>

</body>