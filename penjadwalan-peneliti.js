const baseUrl = "/api_penelitian/";
const form = $("#form_jadwal");

$(document).ready(function () {
  console.log("ready");

  /** Display all data */
  getAllData(baseUrl + "penelitian/penjadwalan");

  // Cari peneliti
  $("#peneliti").select2({
    width: "100%",
    theme: "bootstrap4",
    placeholder: "Cari nama peneliti...",
    minimumInputLength: 3,
    ajax: {
      url: baseUrl + "penelitian/search_peneliti",
      dataType: "json",
      delay: "250",
      data: function (params) {
        let query = {
          nama: params.term,
        };

        return query;
      },
      processResults: function (data) {
        let temp = data.data;

        let results = temp.map((arr) => {
          return {
            id: arr.id,
            text: `${arr.nik} - ${arr.nama_peneliti}`,
          };
        });

        return {
          results: results,
        };
      },
    },
  });

  // Cari pegawai
  $("#pembimbing").select2({
    width: "100%",
    theme: "bootstrap4",
    placeholder: "Cari nama pegawai...",
    minimumInputLength: 3,
    ajax: {
      url: baseUrl + "master/pegawai",
      dataType: "json",
      delay: "250",
      data: function (params) {
        let query = {
          nama: params.term,
        };

        return query;
      },
      processResults: function (data) {
        let temp = data.data;

        let results = temp.map((arr) => {
          return {
            id: arr.id_peg,
            text: `${arr.nip} - ${arr.nama_lengkap}`,
          };
        });

        return {
          results: results,
        };
      },
    },
  });

  // Cari ruangan
  $("#ruangan").select2({
    width: "100%",
    theme: "bootstrap4",
    placeholder: "Cari ruangan...",
    minimumInputLength: 3,
    ajax: {
      url: baseUrl + "master/unit_rs",
      dataType: "json",
      delay: "250",
      data: function (params) {
        let query = {
          unit: params.term,
        };

        return query;
      },
      processResults: function (data) {
        return {
          results: data.data,
        };
      },
    },
  });

  /** Create Action */
  $(".button-create").click(function () {
    $(".button-modal").addClass("button-store").removeClass("button-update");
    $("#peneliti").attr("disabled", false);
    form[0].reset();
  });

  /** Store Action */
  $("#modal").on("click", ".button-store", function (e) {
    e.preventDefault();
    const formData = form.serializeArray();
    const splitPeneliti = $("#peneliti :selected").text().split("-");

    const otherData = {
      peneliti_nik: splitPeneliti[0].trim(),
      peneliti_nama: splitPeneliti[1].trim(),
      ruangan: $("#ruangan :selected").text(),
    };

    let pembimbing = [];

    $("#daftar_pembimbing tr.pembimbing-created").each(function (index) {
      pembimbing.push({
        pembimbing_id: $(this).find(`#pegawai_id_${index}`).text(),
        pembimbing_nip: $(this).find(`#pegawai_nip_${index}`).text(),
        pembimbing_nama: $(this).find(`#pegawai_nama_${index}`).text(),
      });
    });

    const requestData = formData.reduce((all, curr) => {
      const obj = Object.create({});
      obj[curr.name] = curr.value;

      return {
        ...all,
        ...obj,
        ...otherData,
        pembimbing,
      };
    }, {});

    console.log(requestData);

    Swal.fire({
      title: "Apakah anda yakin?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "Batal",
      confirmButtonText: "Simpan!",
    }).then((response) => {
      if (response.value) {
        store(baseUrl + "penelitian/penjadwalan", requestData);
      }
    });
  });

  /** Edit Action */
  let userId;
  $("#tabel_jadwal").on("click", ".button-edit", function (e) {
    e.preventDefault();
    $(".button-modal").removeClass("button-store");
    $(".button-modal").addClass("button-update");
    userId = $(this).data("id");

    /** Get data for edit form */
    getDataById(baseUrl + "penelitian/penjadwalan/" + userId);
  });

  /** Update Action */
  $("#modal").on("click", ".button-update", function (e) {
    e.preventDefault();
    const formData = form.serializeArray();
    const splitPeneliti = $("#peneliti :selected").text().split("-");

    const otherData = {
      peneliti_nik: splitPeneliti[0].trim(),
      peneliti_nama: splitPeneliti[1].trim(),
      ruangan: $("#ruangan :selected").text(),
    };

    let pembimbing = [];

    $("#daftar_pembimbing tr.pembimbing-created").each(function (index) {
      pembimbing.push({
        pembimbing_id: $(this).find(`#pegawai_id_${index}`).text(),
        pembimbing_nip: $(this).find(`#pegawai_nip_${index}`).text(),
        pembimbing_nama: $(this).find(`#pegawai_nama_${index}`).text(),
        status: $(this).find(`#pegawai_status_${index}`).text(),
      });
    });

    $("#daftar_pembimbing tr.pembimbing-destroyed").each(function (index) {
      pembimbing.push({
        pembimbing_id: $(this).find(`#pegawai_id_${index}`).text(),
        pembimbing_nip: $(this).find(`#pegawai_nip_${index}`).text(),
        pembimbing_nama: $(this).find(`#pegawai_nama_${index}`).text(),
        status: $(this).find(`#pegawai_status_${index}`).text(),
        trans_id: $(this).find(`#trans_id_${index}`).text(),
      });
    });

    const requestData = formData.reduce((all, curr) => {
      const obj = Object.create({});
      obj[curr.name] = curr.value;

      return {
        ...all,
        ...obj,
        ...otherData,
        pembimbing,
      };
    }, {});

    console.log(requestData);

    Swal.fire({
      title: "Apakah anda yakin?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "Batal",
      confirmButtonText: "Simpan!",
    }).then((response) => {
      if (response.value) {
        update(baseUrl + "penelitian/penjadwalan/" + userId, requestData);
      }
    });
  });

  /** Delete Action */
  $("#tabel_jadwal").on("click", ".button-delete", function () {
    const userId = $(this).data("id");

    Swal.fire({
      title: "Apakah anda yakin untuk menghapus data ini?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      confirmButtonText: "Ya, Hapus!",
      cancelButtonColor: "#d33",
      cancelButtonText: "Batal",
    }).then((response) => {
      if (response.value) {
        destroy(`${baseUrl}penelitian/penjadwalan/${userId}`, {
          deleted_by: $("#user_id").val(),
        });
      }
    });
  });

  // Add new pembimbing
  $("#modal").on("click", "#button_tambah_pembimbing", function (e) {
    e.preventDefault();
    const row = $("#daftar_pembimbing tr.pembimbing-created").length;
    const pembimbing = $("#pembimbing :selected").text().split("-");
    const pembimbingId = $("#pembimbing").val();

    if (!pembimbingId) {
      Swal.fire("Terjadi Kesalahan!", "Nama pegawai tidak ditemukan.", "error");
      throw "error: pegawai not found.";
    }

    let data = {
      pembimbing_id: $("#pembimbing").val(),
      pembimbing_nip: pembimbing[0].trim(),
      pembimbing_nama: pembimbing[1].trim(),
    };

    addRow(data, row, "baru");

    $("#pembimbing").val("").change();
  });

  // Remove pembimbing
  $("#tabel_pembimbing").on("click", ".button-hapus-pembimbing", function (e) {
    e.preventDefault();

    $(this)
      .closest("tr")
      .attr("style", "display:none;")
      .addClass("pembimbing-destroyed")
      .removeClass("pembimbing-created");

    $(this).closest("tr").find(".pegawai-status").text("hapus");

    $("#daftar_pembimbing tr.pembimbing-created").each(function (index) {
      $(this).find(".pegawai-nip").attr("id", `pegawai_nip_${index}`);
      $(this).find(".pegawai-nama").attr("id", `pegawai_nama_${index}`);
      $(this).find(".pegawai-id").attr("id", `pegawai_id_${index}`);
      $(this).find(".pegawai-status").attr("id", `pegawai_status_${index}`);
    });

    $("#daftar_pembimbing tr.pembimbing-destroyed").each(function (index) {
      $(this).find(".pegawai-nip").attr("id", `pegawai_nip_${index}`);
      $(this).find(".pegawai-nama").attr("id", `pegawai_nama_${index}`);
      $(this).find(".pegawai-id").attr("id", `pegawai_id_${index}`);
      $(this).find(".pegawai-status").attr("id", `pegawai_status_${index}`);
      $(this).find(".trans-id").attr("id", `trans_id_${index}`);
    });
  });
});

function category(value) {
  let categoryName;

  switch (value) {
    case "1":
      categoryName = "Kedokteran";
      return categoryName;
    case "2":
      categoryName = "Keperawatan";
      return categoryName;
    case "3":
      categoryName = "Kebidanan";
      return categoryName;
    case "4":
      categoryName = "Penunjang";
      return categoryName;
  }
}

function type(value) {
  switch (value) {
    case "1":
      return "Data Awal & Residensi";
    case "2":
      return "Penelitian";
    case "3":
      return "Penelitian Tanpa Surat Kelayakan Etik";
    case "4":
      return "Penelitian Dengan Surat Kelayakan Etik";
    case "5":
      return "Penerbitan Surat Kelayakan Etik Tanpa Penelitian";
    default:
      break;
  }
}

function getAllData(url) {
  $.ajax({
    method: "GET",
    url: url,
    dataType: "json",
    success: function (response) {
      if (response.status == 200) {
        let jadwal = response.data;
        jadwal.forEach((value, index) => {
          $("#body").append(`
            <tr>
              <td>${index + 1}.</td>
              <td>${value.nama_peneliti}</td>
              <td>${type(value.jenis)}</td>
              <td>${category(value.kategori)}</td>
              <td>${value.ruangan}</td>
              <td>${value.tgl_mulai}</td>
              <td>${value.tgl_selesai}</td>
              <td>
                <button class="btn btn-sm btn-info button-edit" data-id="${
                  value.jadwal_id
                }">
                  <i class="fas fa-pencil-alt"></i>
                </button>
                <button class="btn btn-sm btn-danger button-delete" data-id="${
                  value.jadwal_id
                }">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </td>
            </tr>
          `);
        });
      }
      $("#tabel_jadwal").DataTable({
        scrollY: "300",
      });
    },
  });
}

function store(url, data) {
  Swal.fire({
    title: "Mohon Tunggu...",
    allowOutsideClick: false,
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: false,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onOpen: () => {
      $.ajax({
        method: "POST",
        url: url,
        data: data,
        dataType: "json",
        success: function (response) {
          if (response.status == 201) {
            Swal.fire({
              icon: "success",
              title: "Data berhasil disimpan.",
              showConfirmButton: true,
            }).then((response) => {
              location.reload();
            });
          } else {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: response.message,
            });
          }
        },
      });
    },
  });
}

function getDataById(url) {
  $.ajax({
    method: "GET",
    url: url,
    dataType: "json",
    success: function (response) {
      if (response.status == 200) {
        $("#daftar_pembimbing").empty();
        let result = response.data;

        // set data peneliti
        const peneliti = {
          id: result[0].peneliti_id,
          text: `${result[0].nik} - ${result[0].nama_peneliti}`,
        };

        // set option
        const penelitiOption = new Option(
          peneliti.text,
          peneliti.id,
          true,
          true
        );

        // set option to select element
        $("#peneliti")
          .append(penelitiOption)
          .trigger("change")
          .attr("disabled", true);

        // set data peneliti
        const ruangan = {
          id: result[0].ruangan_id,
          text: result[0].ruangan,
        };

        // set option
        const ruanganOption = new Option(ruangan.text, ruangan.id, true, true);

        // set option to select element
        $("#ruangan").append(ruanganOption).trigger("change");

        $("#jenis_penelitian").val(result[0].jenis_penelitian);
        $("#kategori").val(result[0].kategori);
        $("#tglMulai").val(result[0].tgl_mulai);
        $("#tglSelesai").val(result[0].tgl_selesai);

        result.forEach((element, index) => {
          addRow(element, index);
        });

        $("#modal").modal("show");
      }
    },
  });
}

function update(url, data) {
  Swal.fire({
    title: "Mohon Tunggu...",
    allowOutsideClick: false,
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: false,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onOpen: () => {
      $.ajax({
        method: "PUT",
        url: url,
        data: JSON.stringify(data),
        dataType: "json",
        success: function (response) {
          if (response.status == 201) {
            Swal.fire({
              icon: "success",
              title: "Data berhasil diupdate.",
              showConfirmButton: true,
            }).then((response) => {
              location.reload();
            });
          } else {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: response.message,
            });
          }
        },
      });
    },
  });
}

function destroy(url = "", data = {}) {
  Swal.fire({
    title: "Mohon Tunggu...",
    allowOutsideClick: false,
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: false,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onOpen: () => {
      $.ajax({
        method: "DELETE",
        url: url,
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
          if (response.status == 200) {
            Swal.fire("Deleted!", response.message, "success").then(
              (response) => {
                location.reload();
              }
            );
          }
        },
      });
    },
  });
}

function getPeneliti(url) {
  $.ajax({
    method: "GET",
    url: url,
    success: function (response) {
      let peneliti = response.data;

      peneliti.forEach((value, key) => {
        $("#penelitiId").append(
          "<option value=" +
            value["id"] +
            ">" +
            value["nik"] +
            " - " +
            value["nama_peneliti"] +
            "</option>"
        );
      });
    },
  });
}

function getPembimbing(url) {
  $.ajax({
    method: "GET",
    url: url,
    success: function (response) {
      let pembimbing = response.data;

      pembimbing.forEach((value) => {
        $("#pembimbing").append(`
          <option value="${value["non_nama"]}" name="${value["non_nik"]}">${value["non_nama"]}</option >
        `);
      });
    },
  });
}

function addRow(data, index, status = "exist") {
  const id = data.pembimbing_id || "";
  const nip = data.pembimbing_nip || "";
  const nama = data.pembimbing_nama || "";
  const trans_id = data.pjb_id || "";

  $("#tabel_pembimbing").find("tbody").append(`
    <tr class="pembimbing pembimbing-created">
      <td class="pegawai-nip" id="pegawai_nip_${index}">${nip}</td>
      <td class="pegawai-nama"id="pegawai_nama_${index}">${nama}</td>
      <td>
        <button class="btn btn-sm btn-danger button-hapus-pembimbing">
          <i class="fas fa-trash"></i>
        </button>
      </td>
      <td class="trans-id" id="trans_id_${index}" style="display:none;">${trans_id}</td>
      <td class="pegawai-id" id="pegawai_id_${index}" style="display:none;">${id}</td>
      <td class="pegawai-status" id="pegawai_status_${index}" style="display:none;">${status}</td>
    </tr>
  `);
}
