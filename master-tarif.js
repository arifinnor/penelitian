const apiUrl = '/api_penelitian';

$(document).ready(function () {
  console.log('ready');
  getPendidikan('class_master.php?get=pendidikan');
  getJenisPenelitian('class_master.php?get=jenis_penelitian');
  getAllTarif(`${apiUrl}/penelitian/tarif`);

  $('.button-create').on('click', function (e) {
    e.preventDefault();
    $('#modal').find('.modal-title').text('Tambah Tarif');
    $('#modal')
      .find('.button-modal')
      .addClass('button-store')
      .removeClass('button-update');
    $('#modal').modal('show');
  });

  $('#modal').on('click', '.button-store', function (e) {
    e.preventDefault();

    const form = $('#form').serializeArray();
    const requestData = form?.reduce(function (all, current) {
      const newObj = Object.create({});
      newObj[current.name] = current.value;

      return {
        ...all,
        ...newObj,
      };
    }, {});

    console.log(requestData);
    // store(`${apiUrl}/penelitian/tarif`, requestData);
  });
});

function getAllTarif(url) {
  let dataTable = $('#tabel_tarif').DataTable({
    ajax: {
      url: url,
      dataSrc: 'data',
    },
    processing: true,
    responsive: true,
  });
}

function getPendidikan(url) {
  $.ajax({
    method: 'GET',
    url: url,
    dataType: 'json',
    success: function (response) {
      if (response) {
        response.forEach(element => {
          $('#pendidikan').append(`
            <option value="${element[0]}">${element[1]}</option>
          `);
        });
      }
    },
  });
}

function getJenisPenelitian(url) {
  $.ajax({
    method: 'GET',
    url: url,
    dataType: 'json',
    success: function (response) {
      if (response) {
        response.forEach(element => {
          $('#penelitian').append(`
            <option value="${element[0]}">${element[1]}</option>
          `);
        });
      }
    },
  });
}

function store(url, data) {
  Swal.fire({
    title: 'Mohon Tunggu...',
    allowOutsideClick: false,
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: false,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onOpen: () => {
      $.ajax({
        method: 'POST',
        url: url,
        data: data,
        dataType: 'json',
        success: function (response) {
          if (response.status == 201) {
            Swal.fire({
              icon: 'success',
              title: 'Data berhasil disimpan.',
              showConfirmButton: true,
            }).then(response => {
              location.reload();
            });
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: response.message,
            });
          }
        },
      });
    },
  });
}
