function checkStatus(value) {
  let status = {};

  if (value == 'Aktif') {
    status.class = 'btn btn-sm btn-block btn-success font-weight-bold';
    status.name = '<i class="fas fa-check-circle"></i>&nbsp;Aktif';
  } else if (value == 'Tidak Aktif') {
    status.class = 'btn btn-sm btn-block btn-danger font-weight-bold';
    status.name = '<i class="fas fa-times-circle"></i>&nbsp;Tidak Aktif'
  }

  return status;
}



function getAllBerkas(url) {
  let i = 1;

  $.ajax({
    method: 'GET',
    url: url,
    success: (response) => {
      let berkas = response.data;

      berkas.forEach((value) => {
        let statusBerkas = checkStatus(value['status_file']);

        $("#berkas-body").append(
          `<tr>
            <td>${i++}</td>
            <td>${value['jenis_file']}</td>
            <td>${value['kategori_file']}</td>
            <td>
              <span class="${statusBerkas.class}">
                ${statusBerkas.name}
              </span>
            </td>
            <td>
              <button class="btn btn-sm btn-info btn-aksi button-edit"
                data-toggle="modal" data-target="#modal-berkas"
                data-id="${value['kd_file']}">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger btn-aksi button-delete"
                data-id="${value['kd_file']}">
                <i class="fas fa-trash-alt"></i>  
              </button>
            </td>
          </tr>`
        );
      });
    }
  });
}



function store(url, data) {
  $.ajax({
    method: 'POST',
    url: url,
    data: data,
    dataType: 'json',
    success: (response) => {
      if (response.status == 201) {
        Swal.fire({
          icon: 'success',
          title: 'Your work has been saved',
          showConfirmButton: true,
          // timer: 1500
        }).then((reponse) => {
          location.reload();
        });
      }
    }
  });
}



function getBerkasById(url) {
  $.ajax({
    method: 'GET',
    url: url,
    success: (response) => {
      let data = response.data;
      $("#nama_berkas").val(data['jenis_file']);
      $("#kategori").val(data['kategori_file']);
      $("#status_berkas").val(data['status_file']);
    }
  });
}



function update(url, data) {
  $.ajax({
    method: "PUT",
    url: url,
    data: JSON.stringify(data),
    dataType: 'json',
    success: (response) => {
      if (response.status == 204) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: response.message
        });
      } else {
        Swal.fire({
          icon: 'success',
          title: 'Your work has been saved',
          showConfirmButton: true

        }).then((response) => {
          location.reload();
        });
      }
    },
    error: function (err) {
      console.log(err);
    }
  });

}

function destroy(url) {
  $.ajax({
    method: 'DELETE',
    url: url,
    dataType: 'json',
    success: (response) => {
      if (response.status == 200) {
        Swal.fire(
          'Deleted!',
          response.message,
          'success',
        ).then((response) => {
          if (response.value) {
            location.reload();

          }
        });
      }
    }
  });
}