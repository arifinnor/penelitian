<?php
session_start();
if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == '') {
  echo "<script>alert('Anda belum login atau session anda habis, silakan login ulang.');
  window.location='/simrs/pendidikan_nondm/';</script>";
}
include '../connect/konek.php';
include '../head_menu.php';
include 'header.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Penerimaan Pembayaran</title>

  <!-- bootstrap 4 -->
  <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <!-- datatables -->
  <link rel="stylesheet" href="assets/plugins/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css">
  <!-- sweetalert2 -->
  <link rel="stylesheet" href="assets/plugins/sweetalert2/sweetalert2.min.css">
  <!-- fontawesome -->
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/fontawesome.css">
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/brands.css">
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/solid.css">
  <!-- my-style -->
  <link rel="stylesheet" href="assets/css/my-style.css">

</head>

<body class="bg-info">
  <div class="container bg-light" style="height: 100%;">
    <div class="text-center title-page">
      <h5><b>.: Penerimaan Pembayaran :.</b></h5>
    </div>
    <div class="m-3">
      <button class="btn btn-sm btn-primary button-create" data-toggle="modal" data-target="#modal-bayar">
        <i class="fas fa-plus"></i>
        Tambah Pembayaran
      </button>
    </div>

    <!-- Table -->
    <div class="m-3">
      <table id="table-bayar" class="table table-sm table-bordered table-striped table-hover" style="width:100%;">
        <thead class="bg-light">
          <tr>
            <th scope="col" class="font-weight-bold">No.</th>
            <th scope="col" class="font-weight-bold">Tgl Terima</th>
            <th scope="col" class="font-weight-bold">NIK</th>
            <th scope="col" class="font-weight-bold">Nama Peneliti</th>
            <th scope="col" class="font-weight-bold">Jenis Bayar</th>
            <th scope="col" class="font-weight-bold">Periode</th>
            <th scope="col" class="font-weight-bold">Nilai Bayar</th>
            <th scope="col" class="font-weight-bold">Verifikasi</th>
            <th scope="col" class="font-weight-bold">Aksi</th>
          </tr>
        </thead>
        <tbody id="bayar-body">
        </tbody>
      </table>
    </div>
  </div>
  <!-- End Table -->

  <!-- Modal -->
  <div class="modal fade" id="modal-bayar" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Detail Pembayaran</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <form id="form-pembayaran">
              <div class="form-group row">
                <label for="nama" class="col-sm-3 col-form-label col-form-label-sm text-right">Nama Peneliti :</label>
                <div class="col-sm-9">
                  <select name="nama" id="nama" class="form-control form-control-sm" data-live-search="true">
                    <option value="">-- Pilih nama peneliti --</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="institusi" class="col-sm-3 col-form-label col-form-label-sm text-right">Asal Institusi :</label>
                <div class="col-sm-9">
                  <input type="text" name="institusi" id="institusi" class="form-control form-control-sm" placeholder="Asal institusi" disabled>
                </div>
              </div>
              <div class="form-group row">
                <label for="jenis" class="col-sm-3 col-form-label col-form-label-sm text-right">Jenis Pembayaran :</label>
                <div class="col-sm-9">
                  <select name="jenis" id="jenis" class="form-control form-control-sm">
                    <option value="">-- Jenis Pembayaran --</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="nama_ruangan" class="col-sm-3 col-form-label col-form-label-sm text-right">Nama Ruangan :</label>
                <div class="col-sm-9">
                  <input type="text" name="nama_ruangan" id="nama_ruangan" class="form-control form-control-sm" placeholder="Nama ruangan" disabled>
                </div>
              </div>
              <div class="form-group row">
                <label for="jml_ruangan" class="col-sm-3 col-form-label col-form-label-sm text-right">Jumlah Ruangan :</label>
                <div class="col-sm-9">
                  <input type="text" name="jml_ruangan" id="jml_ruangan" class="form-control form-control-sm biaya" placeholder="Jumlah ruangan">
                </div>
              </div>
              <div class="form-group row">
                <label for="nilai" class="col-sm-3 col-form-label col-form-label-sm text-right">Nilai Bayar :</label>
                <div class="col-sm-9">
                  <!-- <input type="text" name="nilaiBayar" id="nilaiBayar" class="form-control form-control-sm biaya" placeholder="Nilai bayar" disabled> -->
                  <select name="nilaiBayar" id="nilaiBayar" class="form-control form-control-sm biaya" placeholder="Nilai bayar">
                    <option value="">-- Pilih nilai bayar --</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="durasi" class="col-sm-3 col-form-label col-form-label-sm text-right">Jangka Waktu :</label>
                <div class="col-sm-9">
                  <input type="text" name="durasi" id="durasi" class="form-control form-control-sm biaya" placeholder="Per bulan">
                </div>
              </div>
              <div class="form-group row">
                <label for="jml_tagihan" class="col-sm-3 col-form-label col-form-label-sm text-right">Jumlah Bayar :</label>
                <div class="col-sm-9">
                  <input type="text" name="jml_tagihan" id="jml_tagihan" class="form-control form-control-sm" placeholder="Jumlah tagihan" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label for="no_kwitansi" class="col-sm-3 col-form-label col-form-label-sm text-right">No Kwitansi :</label>
                <div class="col-sm-9">
                  <input type="text" name="no_kwitansi" id="no_kwitansi" class="form-control form-control-sm" placeholder="No Kwitansi">
                </div>
              </div>
              <div class="form-group row">
                <label for="tgl_kwitansi" class="col-sm-3 col-form-label col-form-label-sm text-right">Tgl Kwitansi :</label>
                <div class="col-sm-9">
                  <input type="date" name="tgl_kwitansi" id="tgl_kwitansi" class="form-control form-control-sm">
                </div>
              </div>
              <div class="form-group row">
                <label for="keterangan" class="col-sm-3 col-form-label col-form-label-sm text-right">Keterangan :</label>
                <div class="col-sm-9">
                  <textarea name="keterangan" id="keterangan" class="form-control form-control-sm" placeholder="Keterangan tambahan"></textarea>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" name="btn-simpan" class="btn btn-sm btn-primary button-modal"><i class="fas fa-save"></i> Simpan</button>
          <button type="button" class="btn btn-sm btn-danger btn-close" data-dismiss="modal"><i class="fas fa-redo"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->

  <!-- User-defined JavaScript -->
  <script src="pembayaran-peneliti.js"></script>

  <!-- Bootstrap -->
  <script src="assets/js/jquery-3.4.1.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> -->
  <script src="assets/js/bootstrap.min.js"></script>
  <!-- <script src="assets/js/popper.min.js"></script> -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
  <script src="assets/plugins/DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>

  <!-- Sweet Alert 2 -->
  <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
  <script src="assets/plugins/sweetalert2/sweetalert2.min.js"></script>

  <script>
    const baseUrl = 'http://localhost/api_penelitian/';

    $(document).ready(function() {
      // Get All Data Pembayaran
      getAllPembayaran(baseUrl + 'pembayaran/penelitian');

      // create payment
      $(".button-create").click(function(e) {
        e.preventDefault();
        $(`#nama`).find('option[value!=""]').remove();
        // $('#form-pembayaran')[0].reset();
        $('.button-modal').removeClass('button-update').addClass('button-simpan');
        $('.button-simpan').removeAttr('data-id');

        // Get data peneliti for the form
        getNamaPeneliti();

        // Get data jenis pembayaran
        $("#jenis").empty().append('<option value="">-- Pilih jenis pembayaran --</option>');
        getJenisBayarPenelitian(baseUrl + 'pembayaran/jenis_bayar_penelitian');
        getBiayaPeneliti(baseUrl + 'pembayaran/jenis_bayar_penelitian');

      });

      // Store payment
      $("#modal-bayar").on('click', '.button-simpan', function(e) {
        e.preventDefault();
        let data = $("#form-pembayaran").serializeArray();

        storePayment(baseUrl + 'pembayaran/penelitian', data);

      });

      // close modal
      $("#modal-bayar").on('click', '.btn-close', function(e) {
        // console.log('batal');
        e.preventDefault();
        // ("#modal-bayar").modal('hide');
        $('#form-pembayaran')[0].reset();

      });

      // Edit payment
      $("#table-bayar").on('click', '.button-edit', function(e) {
        e.preventDefault();
        let id = $(this).data('id');
        // console.log(id);
        // $("#nama").find('option').remove();
        $(".button-modal").removeClass('button-simpan').addClass('button-update');
        $(".button-update").attr('data-id', id);
        $("#modal-bayar").modal('show');

        getPembayaranById(baseUrl + 'pembayaran/penelitian/' + id);

      });

      // Update payment
      $("#modal-bayar").on('click', '.button-update', function(e) {
        e.preventDefault();
        let id = $(this).data('id');

        let data = {
          'nama': $("#nama").val(),
          'jml_ruangan': $("#jml_ruangan").val(),
          'durasi': $("#durasi").val(),
          'jml_tagihan': $("#jml_tagihan").val(),
          'no_kwitansi': $("#no_kwitansi").val(),
          'tgl_kwitansi': $("#tgl_kwitansi").val(),
          'keterangan': $("#keterangan").val()

        }

        updatePayment(baseUrl + 'pembayaran/penelitian/' + id, data);

      });

      // Delete payment
      $("#table-bayar").on('click', '.button-delete', function() {
        let id = $(this).data('id');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((response) => {
          if (response.value) {
            destroyPayment(baseUrl + 'pembayaran/penelitian/' + id);
          }
        });
      });

      // Count total payment
      getTotalBayar();

      // 
      $('#modal-bayar').on('change', '#jenis', function() {
        let nilaiBayar = $(this).data('nilai');
        let id = this.value;
        console.log(nilaiBayar);
        $('#nilaiBayar').val(nilaiBayar);

      });

    })
  </script>