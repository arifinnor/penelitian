<?php
    $host = '/simrs/pendidikan_nondm/penelitian';
?>
  
  <!-- bootstrap 4 -->
  <link rel="stylesheet" href=<?php echo $host ."/assets/css/bootstrap.min.css" ?>>
  <!-- datatables -->
  <link rel="stylesheet" href=<?php echo $host ."/assets/plugins/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css"?>>
  <!-- sweetalert2 -->
  <link rel="stylesheet" href=<?php echo $host ."/assets/plugins/sweetalert2/sweetalert2.min.css" ?>>
  <!-- fontawesome -->
  <link rel="stylesheet" href=<?php echo $host ."/assets/plugins/fontawesome-5.12.1/css/fontawesome.css" ?>>
  <link rel="stylesheet" href=<?php echo $host ."/assets/plugins/fontawesome-5.12.1/css/brands.css" ?>>
  <link rel="stylesheet" href=<?php echo $host ."/assets/plugins/fontawesome-5.12.1/css/solid.css" ?>>

  <link rel="stylesheet" href=<?php echo $host . "/assets/plugins/select2/css/select2.css" ?>>
  <link rel="stylesheet" href=<?php echo $host . "/assets/plugins/select2/css/select2-bootstrap4.css" ?>>

  <!-- my-style -->
  <link rel="stylesheet" href=<?php echo $host ."/assets/css/my-style.css" ?>>