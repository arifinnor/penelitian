<?php
    $host = '/simrs/pendidikan_nondm/penelitian';
?>

<script src=<?php echo $host . "/config/app.js" ?>></script>
<script src=<?php echo $host . "/assets/js/jquery-3.4.1.min.js"?>></script>
<!-- <script src="assets/js/popper.min.js"></script> -->
<script src=<?php echo $host . "/assets/js/bootstrap.min.js" ?>></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src=<?php echo $host . "/assets/plugins/DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js"?>></script>
<script src=<?php echo $host . "/assets/plugins/DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"?>></script>

<script src=<?php echo $host . "/assets/plugins/select2/js/select2.js" ?>></script>

<script src=<?php echo $host . "/assets/plugins/sweetalert2/sweetalert2.all.min.js"?>></script>
<script src=<?php echo $host . "/assets/plugins/sweetalert2/sweetalert2.min.js" ?>></script>

<script src=<?php echo $host . "/assets/plugins/momentjs/moment.min.js" ?>></script>

<script src="index.js"></script>