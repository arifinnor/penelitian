<?php
include '../core/app.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Data Peneliti</title>
  <?php include '../core/links.php' ?>

</head>

<body>
  <!-- Content -->
  <div class="container bg-light" style="height: 100%;">
    <div class="row">
      <div class="col-sm">
        <button class="my-3 btn btn-sm btn-primary button-create" data-toggle="modal" data-target="#modal">
          <i class="fas fa-plus-circle"></i>
          Tambah Baru
        </button>
      </div>
    </div>
    <!-- Table -->
    <div class="row">
      <div class="col-sm">
        <div class="card">
          <div class="card-body">
            <table id="myTable" class="table table-sm table-striped table-hover" style="width:100%;">
              <thead class="bg-light">
                <tr>
                  <th class="font-weight-bold">No.</th>
                  <th class="font-weight-bold">NIK</th>
                  <th class="font-weight-bold">Nama Peneliti</th>
                  <th class="font-weight-bold">Asal Instansi</th>
                  <th class="font-weight-bold text-center">Status</th>
                  <th class="font-weight-bold text-right">Aksi</th>
                </tr>
              </thead>
              <tbody id="body">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- End Table -->
  </div>
  <!-- End Content -->

  <!-- Modal -->
  <div id="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Data Peneliti</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container">
            <form id="form">
              <!-- Section 0 -->
              <div class="card border-primary mb-3">
                <div class="card-header bg-primary text-white text-center">
                  <h5><i class="fas fa-book"></i> Status Penelitian</h5>
                </div>
                <div class="card-body">
                  <!-- <div class="form-group row">
                    <label for="jenisPenelitian" class="col-sm-3 col-form-label text-right">Jenis Penelitian :</label>
                    <div class="col-sm-9">
                      <select name="jenisPenelitian" id="jenisPenelitian" class="form-control form-control-sm">
                        <option value="">-- Pilih Jenis Penelitian --</option>
                        <option value="1">Data Awal</option>
                        <option value="2">Penelitian</option>
                      </select>
                    </div>
                  </div> -->
                  <div class="form-group row">
                    <label for="statusPeneliti" class="col-sm-3 col-form-label text-right">Status Peneliti :</label>
                    <div class="col-sm-9">
                      <select name="statusPeneliti" id="statusPeneliti" class="form-control form-control-sm">
                        <option value="">-- Pilih Status Peneliti --</option>
                        <option value="3">Progress - Direktur</option>
                        <option value="4">Progress - Wadir Umum & Pendidikan</option>
                        <option value="5">Progress - Kabag SDM & Diklat</option>
                        <option value="6">Progress - Kasubbag Pelatihan & Pengembangan</option>
                        <option value="7">Progress - Kepala Instalasi/Kepala Ruangan Unit Terkait</option>
                        <option value="1">Aktif</option>
                        <option value="2">Tidak Aktif/Lulus</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Section 1 -->
              <div class="card border-primary mb-3">
                <div class="card-header bg-primary text-white text-center">
                  <h5><i class="fas fa-user"></i> Data Pribadi</h5>
                </div>
                <div class="card-body">
                  <div class="form-group row nik">
                    <label for="nik" class="col-sm-3 col-form-label text-right">NIK :</label>
                    <div class="col-sm-9">
                      <input type="text" name="nik" id="nik" class="form-control form-control-sm" placeholder="Your ID Number Here" maxlength="16" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label text-right">Nama :</label>
                    <div class="col-sm-9">
                      <input type="text" name="nama" id="nama" class="form-control form-control-sm" placeholder="Your Name Here" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="telepon" class="col-sm-3 col-form-label text-right">Nomor Telepon :</label>
                    <div class="col-sm-9">
                      <input type="text" name="telepon" id="telepon" class="form-control form-control-sm" placeholder="Your Phone Number Here" maxlength="12" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="email" class="col-sm-3 col-form-label text-right">Alamat Email :</label>
                    <div class="col-sm-9">
                      <input type=" email" name="email" id="email" class="form-control form-control-sm" placeholder="email@example.com" required="required">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="pob" class="col-sm-3 col-form-label text-right">Tempat Lahir :</label>
                    <div class="col-sm-9">
                      <input type="text" name="pob" id="pob" class="form-control form-control-sm" placeholder="Your Birthplace Here">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="dob" class="col-sm-3 col-form-label text-right">Tanggal Lahir :</label>
                    <div class="col-sm-9">
                      <input type="date" name="dob" id="dob" class="form-control form-control-sm">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="sex" class="col-sm-3 col-form-label text-right">Jenis Kelamin :</label>
                    <div class="col-sm-9">
                      <select name="sex" id="sex" class="form-control form-control-sm">
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Section 2 -->
              <div class="card border-primary mb-3">
                <div class="card-header bg-primary text-white text-center">
                  <h5><i class="fas fa-graduation-cap"></i> Pendidikan Terakhir</h5>
                </div>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="pendidikan" class="col-sm-3 col-form-label text-right">Pendidikan :</label>
                    <div class="col-sm-9">
                      <select name="pendidikan" id="pendidikan" class="form-control form-control-sm">
                        <option value="">-- Choose One --</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="instansi" class="col-sm-3 col-form-label text-right">Asal Instansi :</label>
                    <div class="col-sm-9">
                      <input type="text" name="instansi" id="instansi" class="form-control form-control-sm" placeholder="Your University Here">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="fakultas" class="col-sm-3 col-form-label text-right">Fakultas :</label>
                    <div class="col-sm-9">
                      <input type="text" name="fakultas" id="fakultas" class="form-control form-control-sm" placeholder="Your Faculty Here">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="jurusan" class="col-sm-3 col-form-label text-right">Jurusan :</label>
                    <div class="col-sm-9">
                      <input type="text" name="jurusan" id="jurusan" class="form-control form-control-sm" placeholder="Your Majors Here">
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" name="btn-update" class="btn btn-sm btn-primary button-modal"><i class="fas fa-save"></i> Simpan</button>
          <button type="button" class="btn btn-sm btn-danger btn-close" data-dismiss="modal"><i class="fas fa-redo"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->

  <!-- Modal File -->
  <div class="myModal"></div>
  <!-- End Modal File -->

  <?php include '../core/scripts.php' ?>
</body>

</html>