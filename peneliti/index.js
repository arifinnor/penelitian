$(document).ready(function () {
  /** Display all data */
  getAll(baseUrl + "/peneliti");
  getPendidikan();

  /** Create Action */
  $(".button-create").click(function () {
    $(".button-modal").addClass("button-store");
    $("#form")[0].reset();
  });

  /** Store Action */
  $("#modal").on("click", ".button-store", function (e) {
    e.preventDefault();
    let data = $("#form").serialize();

    store(baseUrl + "/peneliti", data);
  });

  /** Edit Action */
  $("#myTable").on("click", ".button-edit", function (e) {
    e.preventDefault();
    $(".button-modal").removeClass("button-store");
    $(".button-modal").addClass("button-update");

    /** Get data for edit form */
    userId = $(this).data("id");
    getById(baseUrl + "/peneliti/" + userId);
  });

  /** Update Action */
  $("#modal").on("click", ".button-update", function (e) {
    // data request
    let data = {
      nik: $("#nik").val(),
      nama: $("#nama").val(),
      telepon: $("#telepon").val(),
      email: $("#email").val(),
      pob: $("#pob").val(),
      dob: $("#dob").val(),
      sex: $("#sex").val(),
      pendidikan: $("#pendidikan").val(),
      instansi: $("#instansi").val(),
      fakultas: $("#fakultas").val(),
      jurusan: $("#jurusan").val(),
      statusPeneliti: $("#statusPeneliti").val(),
      jenisPenelitian: $("#jenisPenelitian").val(),
    };

    update(baseUrl + "/peneliti/" + userId, data);

    // Get image data for passing into FormData
    let form = $("#formFileUpload")[0];
  });

  /** Delete Action */
  $("#myTable").on("click", ".button-delete", function () {
    userId = $(this).data("id");

    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((response) => {
      if (response.value) {
        destroy(baseUrl + "/peneliti/" + userId);
      }
    });
  });

  // /** Check NIK availability */
  // $("#modal").on("change", "#nik", function () {
  //   let nik = $("#nik").val();
  //   checkNik(baseUrl + "peneliti/nik/" + nik);
  // });

  /** Button file Action */
  $("#myTable").on("click", ".button-file", function () {
    let nik = $(this).data("nik");
    let nama = $(this).data("nama");
    let title = "Daftar Berkas Peneliti " + nama;

    // TODO: Load file list
    getFilePenelitiById(baseUrl + "/peneliti/" + nik + "/berkas");

    // TODO: Load modal
    myViewModal(title, content());

    // TODO: Initialize temporary data
    $("#tmpNik").val(nik);
  });

  /** Button-file-add action */
  $(".myModal").on("click", ".button-file-add", function () {
    let fileId = $(this).data("id");
    let jenisFile = $(this).data("jenis");

    $("#tmpFileId").val(fileId);
    $("#tmpJenisFile").val(jenisFile);

    // changing status column to input file
    $("#status-" + fileId).empty().html(`
    <form id="formUploadLaporan-${fileId}"
      enctype="multipart/form-data"
      class="fileForm">
      <div class="form-group">
        <input name="berkas" type="file"
          class="form-control" />
      </div>
    </form>
    `);

    // changing action column to file upload action
    $("#action-" + fileId).empty().html(`
      <button id="button-file-upload-${fileId}"
        class="btn btn-sm btn-primary button-file-upload
          font-weight-bold btn-status-berkas">
        <i class="fas fa-save"></i>
        Simpan
      </button>
      <br>
      <button class="btn btn-sm btn-warning button-file-cancel
        font-weight-bold text-white btn-status-berkas">
        <i class="fas fa-redo"></i>
        Batal
      </button>
    `);

    $(".button-file-add").attr("disabled", true);
  });

  // TODO: Button-file-upload action
  $(".myModal").on("click", ".button-file-upload", function () {
    let nik = $("#tmpNik").val();
    let fileId = $("#tmpFileId").val();
    let jenisFile = $("#tmpJenisFile").val();

    // Get image data for passing into FormData
    let form = $("#formUploadLaporan-" + fileId)[0];
    let formId = "#formUploadLaporan-" + fileId;

    // Create an FormData object
    let fd = new FormData(form);
    fd.append("nik", nik);
    fd.append("kdFile", fileId);

    // Sending upload request
    fileUpload(baseUrl + "/peneliti/" + nik + "/berkas", fd);
  });

  // TODO: Button-file-delete action
  $(".myModal").on("click", ".button-file-delete", function () {
    let fileId = $(this).data("file");

    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((response) => {
      if (response.value) {
        destroy(baseUrl + "peneliti/berkas/" + fileId);
      }
    });
  });

  // TODO: Button-file-cancel
  $(".myModal").on("click", ".button-file-cancel", function () {
    let nik = $("#tmpNik").val();
    let reloadUrl = baseUrl + "/peneliti/" + nik + "/berkas";
    getFilePenelitiById(reloadUrl);
  });
});

function getAll(url) {
  const dt = $("#myTable").DataTable({
    ajax: url,
    ajaxSource: "data",
    order: [0],
    columns: [
      {
        data: "id",
        width: "1%",
      },
      {
        data: "nik",
        width: "10%",
      },
      {
        data: "nama_peneliti",
        width: "30%",
      },
      {
        data: "instansi",
        width: "20%",
      },
      {
        data: "status_peneliti",
        width: "7%",
        className: "text-center",
        orderable: false,
        searchable: false,
        render: function (data) {
          let badge;
          let text;

          switch (data) {
            case "1":
              badge = "badge-success";
              text = "Aktif";
              break;
            case "2":
              badge = "badge-danger";
              text = "Tidak Aktif";
              break;
            default:
              badge = "badge-info";
              text = "Progress";
              break;
          }

          const status = `<span class="badge ${badge}">${text}</span>`;

          return status;
        },
      },
      {
        data: "id",
        width: "10%",
        className: "text-right",
        orderable: false,
        searchable: false,
        render: function (data) {
          return `
          <button 
            class="btn btn-info btn-sm button-edit" 
            data-id="${data}"
          >
            <i class="fa fa-edit"></i>
          </button>
          <button 
            class="btn btn-danger btn-sm button-delete" 
            data-id="${data}"
          >
            <i class="fa fa-trash"></i>
          </button>
          `;
        },
      },
    ],
  });

  dt.on("order.dt search.dt", function () {
    dt.column(0, { search: "applied", order: "applied" })
      .nodes()
      .each(function (cell, i) {
        cell.innerHTML = i + 1 + ".";
      });
  }).draw();
}

function store(url, data) {
  $.ajax({
    method: "POST",
    url: url,
    data: data,
    dataType: "json",
    success: function (response) {
      if (response.status == 201) {
        Swal.fire({
          icon: "success",
          title: "Your work has been saved",
          showConfirmButton: true,
          // timer: 1500
        }).then((reponse) => {
          location.reload();
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: response.message,
        });
      }
    },
  });
}

function getById(url) {
  $.ajax({
    method: "GET",
    url: url,
    success: function (response) {
      $("#modal").modal("show");

      let result = response.data;

      $("#nik").val(result["nik"]);
      $("#nama").val(result["nama_peneliti"]);
      $("#telepon").val(result["telepon"]);
      $("#email").val(result["email"]);
      $("#pob").val(result["pob"]);
      $("#dob").val(result["dob"]);
      $("#sex").val(result["sex"]);
      $("#pendidikan").val(result["pendidikan"]);
      $("#instansi").val(result["instansi"]);
      $("#fakultas").val(result["fakultas"]);
      $("#jurusan").val(result["jurusan"]);
      $("#statusPeneliti").val(result["status_peneliti"]);
      $("#jenisPenelitian").val(result["jenis_penelitian"]);
    },
  });
}

function update(url, data) {
  $.ajax({
    method: "PUT",
    url: url,
    data: JSON.stringify(data),
    dataType: "json",
    success: function (result) {
      if (result.status == 204) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: result.message,
        });
      } else {
        Swal.fire({
          icon: "success",
          title: "Your work has been saved",
          showConfirmButton: true,
        }).then((result) => {
          location.reload();
        });
      }
    },
    error: function (err) {
      console.log(err);
    },
  });
}

function destroy(url) {
  let nik = $("#tmpNik").val();
  let reloadUrl = baseUrl + "/peneliti/" + nik + "/berkas";

  $.ajax({
    method: "DELETE",
    url: url,
    dataType: "json",
    success: function (response) {
      if (response.status == 200 && response.type == "file") {
        Swal.fire("Deleted!", response.message, "success").then((response) => {
          if (response.value) {
            getFilePenelitiById(reloadUrl);
          }
        });
      } else if (response.status == 200) {
        Swal.fire("Deleted!", response.message, "success").then((response) => {
          if (response.value) {
            location.reload();
          }
        });
      }
    },
  });
}

function softDelete(url) {
  $.ajax({
    method: "POST",
    url: url,
    dataType: "json",
    success: function (response) {
      if (response.status == 200) {
        Swal.fire("Deleted!", response.message, "success").then((response) => {
          location.reload();
        });
      }
    },
  });
}

function getPendidikan() {
  $.ajax({
    method: "GET",
    url: "/simrs/pendidikan_nondm/penelitian/class_master.php?get=pendidikan",
    success: function (result) {
      let data = JSON.parse(result);

      data.forEach((value, key) => {
        $("#pendidikan").append(
          "<option value='" + value[0] + "'>" + value[1] + "</option>"
        );
      });
    },
  });
}

function checkFileName(value) {
  if (value == 1) {
    persyaratan = `<small class="form-text text-muted">Surat permohonan izin penelitian</small>`;
  } else if (value == 2) {
    persyaratan = `<small class="form-text text-muted">Proposal asli yang sudah disetujui oleh pembimbing</small>`;
  } else if (value == 3) {
    persyaratan = `<small class="form-text text-muted">Ethical clearence</small>`;
  } else if (value == 4) {
    persyaratan = `<small class="form-text text-muted">Surat pengantar Bakesbangpol</small>`;
  } else if (value == 5) {
    persyaratan = `<small class="form-text text-muted">Surat permohonan dosen pembimbing klinis/lapangan</small>`;
  }

  return persyaratan;
}

function checkNik(url) {
  $.ajax({
    method: "GET",
    url: url,
    success: (response) => {
      if (response.status == "used") {
        $("#used").remove();
        $(".nik").append(`
          <div class="col-sm-9 offset-sm-3 alert alert-warning" id="used" role="alert">
            <strong>NIK</strong> sudah digunakan, harap diperiksa kembali.
          </div>
        `);

        $(".button-store").attr("disabled", true);

        // $("#nik").attr({
        //   "data-toggle": "popover",
        //   "title": "Popover Header",
        //   "data-content": "Some content inside the popover"
        // });

        // $("#nik").popover();
      } else {
        $("#used").remove();
        $(".button-store").attr("disabled", false);
      }
    },
  });
}

function checkStatus(value) {
  let status = {};

  if (value == "1") {
    status.class = "btn btn-sm btn-block btn-success font-weight-bold";
    status.name = '<i class="fas fa-check-circle"></i>&nbsp;Aktif';
  } else if (value == "2") {
    status.class = "btn btn-sm btn-block btn-danger font-weight-bold";
    status.name = '<i class="fas fa-times-circle"></i>&nbsp;Tidak Aktif';
  } else {
    status.class =
      "btn btn-sm btn-block btn-warning text-white font-weight-bold";
    status.name = '<i class="fas fa-question-circle"></i>&nbsp;Progress';
  }

  return status;
}

function fileUpload(url, data) {
  let nik = $("#tmpNik").val();
  let reloadUrl = baseUrl + "/peneliti/" + nik + "/berkas";

  Swal.fire({
    title: "Uploading...",
    allowOutsideClick: false,
    showCloseButton: true,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onOpen: () => {
      $.ajax({
        url: url,
        data: data,
        method: "POST",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        success: (response) => {
          if (response.status == 204) {
            Swal.hideLoading();
            Swal.fire("Upload Failed");
          }

          if (response.status == 201) {
            Swal.hideLoading();
            Swal.fire("Upload Success").then((result) => {
              if (result.value) {
                getFilePenelitiById(reloadUrl);
              }
            });
          }
        },
      });
    },
  });
}

function myViewModal(title, content) {
  $(".myModal").html(`
    <div id="fileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">${title}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
              <span>&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container contents">
              ${content}
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-danger btn-close" data-dismiss="modal"><i class="fas fa-redo"></i> Batal</button>
          </div>
        </div>
      </div>
    </div>
    `);
}

function checkStatusBerkas(value) {
  let status = {};

  if (value === undefined || value == null) {
    status.class = `class="btn btn-sm btn-danger text-white font-weight-bold btn-status-berkas"`;
    status.name = `<i class="fas fa-times-circle"></i>&nbsp;Belum`;
    status.actionClass = `class="btn btn-sm btn-info button-file-add font-weight-bold btn-status-berkas"`;
    status.actionName = `<i class="fas fa-file-upload"></i>&nbsp;Upload`;
  } else {
    status.class = `class="btn btn-sm btn-success button-file-status font-weight-bold text-white btn-status-berkas"`;
    status.name = `<i class="fas fa-file-download"></i>&nbsp;Sudah`;
    status.actionClass = `class="btn btn-sm btn-danger button-file-delete font-weight-bold btn-status-berkas"`;
    status.actionName = `<i class="fas fa-trash"></i>&nbsp;Hapus`;
    status.href = `href="${value}" target="_blank"`;
  }

  return status;
}

function content() {
  const isi = `
    <table class="table table-sm table-bordered table-hover" >
      <thead>
        <tr>
          <th class="font-weight-bold" style="width:5%">#</th>
          <th class="font-weight-bold" style="width:35%">Nama Berkas</th>
          <th class="text-center font-weight-bold" style="width:15%;">Jenis Berkas</th>
          <th class="text-center font-weight-bold" style="width:25%;">Status</th>
          <th class="text-right font-weight-bold" style="width:10%">Aksi</th>
        </tr>
      </thead>
      <tbody id="contentBody">
      </tbody>
    </table>
      <div>
        <input id="tmpNik" type="text" style="display:none;">
        <input id="tmpFileId" type="text" style="display:none;">
    </div>`;

  return isi;
}

function getFilePenelitiById(url) {
  $.ajax({
    method: "GET",
    url: url,
    success: function (response) {
      if (response) {
        let data = response.data;
        let i = 1;

        $("#contentBody").empty();

        data.forEach(function (value) {
          let status = checkStatusBerkas(value["path_file"]);

          $("#contentBody").append(`
            <tr>
              <td style="width:10px;">${i++}</td>
              <td style="width:20px;">${value["jenis_file"]}</td>
              <td style="width:10px;" class="text-center">${
                value["kategori_file"]
              }</td>
              <td id="status-${value["kd_file"]}" class="text-center">
                <a
                  ${status.href}
                  ${status.class}
                  data-file="${value["id"]}"
                >
                  ${status.name}
                </a>
              </td>
              <td id="action-${value["kd_file"]}"
                class="text-right">
                <button
                  ${status.actionClass}
                  data-id="${value["kd_file"]}"
                  data-file="${value["id"]}"
                  data-jenis="${value["jenis_file"]}"
                >
                  ${status.actionName}
                </button>
              </td>
            </tr>

            `);
        });
      }
    },
  });
}

function confirm() {
  Swal.fire({
    title: "Apakah anda yakin untuk menyimpan?",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    cancelButtonText: "Batal",
    confirmButtonText: "Simpan!",
  }).then((response) => {
    if (response.value) {
      Swal.fire({
        title: "Sedang menyimpan tagihan...",
        allowOutsideClick: false,
        showConfirmButton: false,
        willOpen: () => {
          Swal.showLoading();
        },
        didOpen: () => {
          //
        },
      });
    }
  });
}
