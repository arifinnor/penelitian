<?php

class Master
{
    function __construct()
    {
        $hostname_conn = "localhost";
        $database_conn = "db_pendidikan_nondm";
        $username_conn = "root";
        $password_conn = "";
        $konek = mysql_connect($hostname_conn, $username_conn, $password_conn);
        mysql_select_db($database_conn, $konek);
    }

    function getPendidikan()
    {
        $query = "SELECT kd_pend, pend FROM ms_pendidikan";
        $exec = mysql_query($query);
        $data = array();

        while ($row = mysql_fetch_row($exec)) {
            array_push($data, $row);
        }
        return json_encode($data);
    }

    function getJenisPenelitian()
    {
        $query = "SELECT id, nama FROM peneliti_jenis_penelitian";
        $exec = mysql_query($query);
        $data = array();

        while ($row = mysql_fetch_row($exec)) {
            array_push($data, $row);
        }
        return json_encode($data);
    }
}

if ($_GET['get'] == 'pendidikan') {
    $data = new Master();
    echo $data->getPendidikan();
}

if ($_GET['get'] == 'jenis_penelitian') {
    $data = new Master();
    echo $data->getJenisPenelitian();
}
