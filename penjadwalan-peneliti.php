<?php
session_start();
if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == '') {
  echo "<script>alert('Anda belum login atau session anda habis, silakan login ulang.');
                        window.location='/simrs/pendidikan_nondm/';
                        </script>";
}
include '../connect/konek.php';
include '../head_menu.php';
include 'header.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Penjadwalan Peneliti</title>

  <!-- bootstrap 4 -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <!-- datatables -->
  <link rel="stylesheet" href="assets/plugins/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css">
  <!-- sweetalert2 -->
  <link rel="stylesheet" href="assets/plugins/sweetalert2/sweetalert2.min.css">
  <!-- fontawesome -->
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/fontawesome.css">
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/brands.css">
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/solid.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../assets/plugins/select2/css/select2.css">
  <link rel="stylesheet" href="../assets/plugins/select2/css/select2-bootstrap4.css">
  <!-- my-style -->
  <link rel="stylesheet" href="assets/css/my-style.css">

</head>

<body>
  <div class="container bg-light" style="height: 100%;">
    <div class="text-center">
      <h5><b>.: Penjadwalan Peneliti :.</b></h5>
    </div>
    <div class="m-3">
      <button class="btn btn-primary button-create" data-toggle="modal" data-target="#modal">
        <i class="fas fa-plus-circle"></i>
        Buat Jadwal
      </button>
    </div>

    <!-- Table -->
    <div class="m-3">
      <table id="tabel_jadwal" class="table table-sm table-bordered table-striped table-hover" style="width:100%;">
        <thead class="bg-light">
          <tr>
            <th scope="col" class="font-weight-bold" style="width: 5%;">No</th>
            <th scope="col" class="font-weight-bold" style="width: 25%;">Nama Peneliti</th>
            <th scope="col" class="font-weight-bold">Jenis Penelitian</th>
            <th scope="col" class="font-weight-bold">Kategori</th>
            <th scope="col" class="font-weight-bold">Ruangan</th>
            <th scope="col" class="font-weight-bold" style="width: 9%;">Tanggal Mulai</th>
            <th scope="col" class="font-weight-bold" style="width: 9%;">Tanggal Selesai</th>
            <th scope="col" class="font-weight-bold" style="width: 9%;">Aksi</th>
          </tr>
        </thead>
        <tbody id="body"></tbody>
      </table>
    </div>
  </div>
  <!-- End Table -->

  <!-- Modal -->
  <div class="modal fade" id="modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Penjadwalan Peneliti</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <form id="form_jadwal">
              <input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['user_id'] ?>">
              <div class="form-group row">
                <label for="peneliti" class="col-sm-3 col-form-label col-form-label-sm text-right">Nama Peneliti :</label>
                <div class=" col-sm-9">
                  <select id="peneliti" name="peneliti_id" class="form-control" required>
                    <option value="">-- Pilih Nama Peneliti --</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="jenis_penelitian" class="col-sm-3 col-form-label col-form-label-sm text-right">Jenis Penelitian :</label>
                <div class="col-sm-9">
                  <select name="jenis" id="jenis_penelitian" class="form-control">
                    <option value="">-- Pilih jenis penelitian --</option>
                    <option value="1">Data Awal & Residensi</option>
                    <option value="2">Penelitian</option>
                    <option value="3">Penelitian Tanpa Surat Kelayakan Etik</option>
                    <option value="4">Penelitian Dengan Surat Kelayakan Etik</option>
                    <option value="5">Penerbitan Surat Kelayakan Etik Tanpa Penelitian</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="kategori" class="col-sm-3 col-form-label col-form-label-sm text-right">Kategori :</label>
                <div class="col-sm-9">
                  <select name="kategori" id="kategori" class="form-control">
                    <option value="">-- Pilih Kategori --</option>
                    <option value="1">Kedokteran</option>
                    <option value="2">Keperawatan</option>
                    <option value="3">Kebidanan</option>
                    <option value="4">Penunjang</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="ruangan" class="col-sm-3 col-form-label col-form-label-sm text-right">Ruangan :</label>
                <div class="col-sm-9">
                  <!-- <input type="text" name="ruangan" id="ruangan" class="form-control" required> -->
                  <select name="ruangan_id" id="ruangan" class="form-control" required>
                    <option value="" hidden></option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="tglMulai" class="col-sm-3 col-form-label col-form-label-sm text-right">Tanggal Mulai :</label>
                <div class="col-sm-6 form-group">
                  <input type="date" name="tgl_mulai" id="tglMulai" class="form-control tgl" required>
                </div>
              </div>
              <div class="form-group row">
                <label for="tglSelesai" class="col-sm-3 col-form-label col-form-label-sm text-right">Tanggal Selesai :</label>
                <div class="col-sm-6 form-group">
                  <input type="date" name="tgl_selesai" id="tglSelesai" class="form-control tgl" required>
                </div>
              </div>
            </form>
            <form>
              <div class="form-group row">
                <label for="pembimbing" class="col-sm-3 col-form-label col-form-label-sm text-right">Nama Pembimbing :</label>
                <div class="col-sm-9">
                  <select id="pembimbing" class="form-control" required>
                    <option value="">-- Pilih Nama Pembimbing --</option>
                  </select>
                  <button class="btn btn-sm btn-success mt-2" id="button_tambah_pembimbing">
                    <span class="fas fa-plus-circle"></span> Tambah
                  </button>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-3 col-sm-9">
                  <div class="table-responsive">
                    <table class="table table-sm table-striped table-bordered table-hover" id="tabel_pembimbing" style="width: 100%;">
                      <thead>
                        <tr>
                          <th style="width: 33%;">NIP</th>
                          <th>Nama Pembimbing</th>
                          <th style="width: 7%; text-align: center">#</th>
                        </tr>
                      </thead>
                      <tbody id="daftar_pembimbing"></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" name="btn-update" class="btn btn-sm btn-primary button-modal">
            <i class="fas fa-save"></i> Simpan
          </button>
          <button type="button" class="btn btn-sm btn-danger btn-close" data-dismiss="modal">
            <i class="fas fa-redo"></i> Batal
          </button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->
  <script src="assets/js/jquery-3.4.1.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="assets/plugins/DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/plugins/select2/js/select2.js"></script>
  <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
  <script src="assets/plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="penjadwalan-peneliti.js"></script>
</body>

</html>