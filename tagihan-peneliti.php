<?php
session_start();
if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == '') {
    echo "<script>alert('Anda belum login atau session anda habis, silakan login ulang.');
  window.location='/simrs/pendidikan_nondm/';</script>";
}
include '../connect/konek.php';
include '../head_menu.php';
include 'header.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tagihan</title>

    <!-- bootstrap 4 -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- datatables -->
    <link rel="stylesheet" href="assets/plugins/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css">
    <!-- sweetalert2 -->
    <link rel="stylesheet" href="assets/plugins/sweetalert2/sweetalert2.min.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/fontawesome.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/brands.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/solid.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../assets/plugins/select2/css/select2.css">
    <link rel="stylesheet" href="../assets/plugins/select2/css/select2-bootstrap4.css">
    <!-- my-style -->
    <link rel="stylesheet" href="assets/css/my-style.css">
    <!-- my-style -->
    <link rel="stylesheet" href="assets/css/my-style.css">

</head>

<body class="bg-info">
    <div class="container bg-light" style="height: 100%;">
        <div class="text-center title-page">
            <h5><b>.: Tagihan :.</b></h5>
        </div>
        <div class="m-3">
            <button class="btn btn-sm btn-primary button-create" data-toggle="modal" data-target="#modal-bayar">
                <i class="fas fa-plus-circle"></i>
                Buat Tagihan
            </button>
        </div>

        <!-- Table -->
        <div class="m-3">
            <table id="table-bayar" class="table table-sm table-hover" style="width:100%;">
                <thead class="bg-light">
                    <tr>
                        <th scope="col" class="font-weight-bold">No.</th>
                        <th scope="col" class="font-weight-bold">Tgl Tagihan</th>
                        <th scope="col" class="font-weight-bold">No. Tagihan</th>
                        <th scope="col" class="font-weight-bold">NIK</th>
                        <th scope="col" class="font-weight-bold">Nama Peneliti</th>
                        <th scope="col" class="font-weight-bold">Jenis Penelitian</th>
                        <th scope="col" class="font-weight-bold">Nilai Tagihan</th>
                        <th scope="col" class="font-weight-bold">Status</th>
                        <th scope="col" class="font-weight-bold">Aksi</th>
                    </tr>
                </thead>
                <tbody id="bayar-body">
                </tbody>
            </table>
        </div>
    </div>
    <!-- End Table -->

    <!-- Modal -->
    <div class="modal fade" id="modal-bayar" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <form id="form-pembayaran">
                            <div class="form-group row">
                                <label for="tgl_tagihan" class="col-sm-3 col-form-label col-form-label-sm text-right">Tgl Tagihan :</label>
                                <div class="col-sm-9">
                                    <input type="date" name="tgl_tagihan" id="tgl_tagihan" class="form-control form-control-sm" value="<?php echo date('Y-m-d') ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="no_tagihan" class="col-sm-3 col-form-label col-form-label-sm text-right">No. Tagihan :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="no_tagihan" id="no_tagihan" class="form-control form-control-sm" placeholder="Nomor Tagihan" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama" class="col-sm-3 col-form-label col-form-label-sm text-right">Nama Peneliti :</label>
                                <div class="col-sm-9">
                                    <select name="nama" id="nama" class="form-control form-control-sm" data-live-search="true"></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis" class="col-sm-3 col-form-label col-form-label-sm text-right">Jenis Penelitian :</label>
                                <div class="col-sm-9">
                                    <select name="jenis" id="jenis" class="form-control form-control-sm">
                                        <option value="" hidden>-- Jenis Penelitian --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="institusi" class="col-sm-3 col-form-label col-form-label-sm text-right">Asal Institusi :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="institusi" id="institusi" class="form-control form-control-sm" placeholder="Asal institusi" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nilai" class="col-sm-3 col-form-label col-form-label-sm text-right">Nilai Bayar :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nilaiBayar" id="nilaiBayar" class="form-control form-control-sm biaya" placeholder="Nilai bayar" readonly>
                                    <!-- <select name="nilaiBayar" id="nilaiBayar" class="form-control form-control-sm biaya" placeholder="Nilai bayar">
                    <option value="">-- Pilih nilai bayar --</option>
                  </select> -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama_ruangan" class="col-sm-3 col-form-label col-form-label-sm text-right">Nama Ruangan :</label>
                                <div class="col-sm-9">
                                    <!-- <input type="text" name="nama_ruangan" id="nama_ruangan" class="form-control form-control-sm" placeholder="Nama ruangan" readonly> -->
                                    <select name="ruangan" id="ruangan" class="form-control form-control-sm">
                                        <option value="" hidden>-- Nama ruangan-- </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jml_ruangan" class="col-sm-3 col-form-label col-form-label-sm text-right">Jumlah Ruangan :</label>
                                <div class="col-sm-9">
                                    <input type="number" min="1" name="jml_ruangan" id="jml_ruangan" class="form-control form-control-sm biaya" placeholder="Jumlah ruangan">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="durasi" class="col-sm-3 col-form-label col-form-label-sm text-right">Jangka Waktu :</label>
                                <div class="col-sm-9">
                                    <div class="input-group input-group-sm">

                                        <input type="text" name="durasi" id="durasi" class="form-control form-control-sm biaya" placeholder="Per bulan" readonly>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="jml_bulan">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jml_tagihan" class="col-sm-3 col-form-label col-form-label-sm text-right">Jumlah Bayar :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="jml_tagihan" id="jml_tagihan" class="form-control form-control-sm" placeholder="Jumlah tagihan" readonly>
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                <label for="no_kwitansi" class="col-sm-3 col-form-label col-form-label-sm text-right">No Kwitansi :</label>
                <div class="col-sm-9">
                  <input type="text" name="no_kwitansi" id="no_kwitansi" class="form-control form-control-sm" placeholder="No Kwitansi">
                </div>
              </div> -->
                            <!-- <div class="form-group row">
                <label for="tgl_kwitansi" class="col-sm-3 col-form-label col-form-label-sm text-right">Tgl Kwitansi :</label>
                <div class="col-sm-9">
                  <input type="date" name="tgl_kwitansi" id="tgl_kwitansi" class="form-control form-control-sm">
                </div>
              </div> -->
                            <div class="form-group row">
                                <label for="keterangan" class="col-sm-3 col-form-label col-form-label-sm text-right">Keterangan :</label>
                                <div class="col-sm-9">
                                    <textarea rows="3" name="keterangan" id="keterangan" class="form-control form-control-sm" placeholder="Keterangan tambahan"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" name="btn-simpan" class="btn btn-sm btn-primary button-modal"><i class="fas fa-save"></i> Simpan</button>
                    <button type="button" class="btn btn-sm btn-danger btn-close" data-dismiss="modal"><i class="fas fa-redo"></i> Batal</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <!-- User-defined JavaScript -->
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="assets/plugins/DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/plugins/select2/js/select2.js"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="tagihan-peneliti.js"></script>

    </script>