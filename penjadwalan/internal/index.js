$(document).ready(function () {
  console.log('ready');
  let penelitianId;

  getAll(baseUrl + '/internal/penjadwalan');

  $('body').on('click', '.button-create', function (e) {
    e.preventDefault();

    $('#myModal').modal('show');
    $('#myModal').find('button.button-modal').addClass('button-store').removeClass('button-update');
  });

  $('#myModal').on('click', '.button-add-peserta', function (e) {
    e.preventDefault();

    const pesertaId = $('#peserta :selected').val();
    const pesertaData = $('#peserta :selected').text();

    const splitted = pesertaData.split('-');

    const data = {
      pegawai_id: pesertaId,
      pegawai_nip: splitted[0].trim(),
      pegawai_nama: splitted[1].trim(),
    };

    const i = $('#table_peserta tr').length;
    addPeserta(data, i);
  });

  $('#myModal').on('click', '.button-delete-peserta', function (e) {
    e.preventDefault();

    $(this).closest('tr').remove();

    $('#table_peserta tbody tr').each(function (index) {
      let noUrut = index + 1;

      $(this).find('span.no-urut').text(`${noUrut}.`);
    });
  });

  $('#myModal').on('click', '.button-delete-peserta', function (e) {
    e.preventDefault();

    $(this).closest('tr').remove();

    $('#table_peserta tbody tr').each(function (index) {
      let noUrut = index + 1;

      $(this).find('span.no-urut').text(`${noUrut}.`);
    });
  });

  $('#myModal').on('click', '.button-store', function (e) {
    e.preventDefault();

    const data = {
      judul: $('#judul').val(),
      kategori: $('#kategori :selected').val(),
      anggaran: $('#anggaran').val(),
      tanggal_mulai: $('#mulai').val(),
      tanggal_selesai: $('#selesai').val(),
      user_id: $('#user_id').val(),
      member: [],
      file: [],
    };

    $('#table_peserta')
      .find('.button-delete-peserta')
      .each(function (index, element) {
        data.member.push({
          pegawai_id: $(this).closest('tr').find('span.peserta-id').text(),
          pegawai_nip: $(this).closest('tr').find('span.peserta-nip').text(),
          pegawai_nama: $(this).closest('tr').find('span.peserta-nama').text(),
        });
      });

    store(`${baseUrl}/internal/penjadwalan`, data);
  });

  $('#myTable').on('click', '.button-edit', function (e) {
    e.preventDefault();
    penelitianId = $(this).data('id');

    getDataById(`${baseUrl}/internal/penjadwalan/${penelitianId}`);

    $('#myModal').modal('show');
    $('#myModal').find('button.button-modal').addClass('button-update').removeClass('button-store');
  });

  $('#myModal').on('click', '.button-update', function (e) {
    e.preventDefault();

    const data = {
      judul: $('#judul').val(),
      kategori: $('#kategori :selected').val(),
      anggaran: $('#anggaran').val(),
      tanggal_mulai: $('#mulai').val(),
      tanggal_selesai: $('#selesai').val(),
      user_id: $('#user_id').val(),
      member: [],
      file: [],
    };

    $('#table_peserta')
      .find('.button-delete-peserta')
      .each(function (index, element) {
        data.member.push({
          pegawai_id: $(this).closest('tr').find('span.peserta-id').text(),
          pegawai_nip: $(this).closest('tr').find('span.peserta-nip').text(),
          pegawai_nama: $(this).closest('tr').find('span.peserta-nama').text(),
        });
      });

    update(`${baseUrl}/internal/penjadwalan/${penelitianId}`, data);
  });

  $('#myTable').on('click', '.button-delete', function (e) {
    e.preventDefault();
    penelitianId = $(this).data('id');

    const data = {
      user_id: $('#user_id').val(),
    };

    destroy(`${baseUrl}/internal/penjadwalan/${penelitianId}`, data);
  });

  $('#peserta').select2({
    width: '100%',
    dropdownParent: $('#myModal'),
    theme: 'bootstrap4',
    placeholder: 'Cari nama pegawai...',
    minimumInputLength: 3,
    ajax: {
      url: apiUrl + '/master/pegawai',
      dataType: 'json',
      delay: '250',
      data: function (params) {
        let query = {
          nama: params.term,
        };

        return query;
      },
      processResults: function (data) {
        let temp = data.data;

        let results = temp.map((arr) => {
          return {
            id: arr.id_peg,
            text: `${arr.nip} - ${arr.nama_lengkap}`,
          };
        });

        return {
          results: results,
        };
      },
    },
  });
});

function getAll(url) {
  const dataTable = $('#myTable').DataTable({
    ajax: url,
    ajaxSource: 'data',
    order: [0],
    columns: [
      { data: 'id' },
      { data: 'judul' },
      { data: 'kategori' },
      { data: 'anggaran' },
      { data: 'tanggal_mulai' },
      { data: 'tanggal_selesai' },
      {
        data: 'id',
        render: function (data) {
          return getActionButton(data);
        },
      },
    ],
  });

  dataTable
    .on('order.dt search.dt', function () {
      dataTable
        .column(0, { search: 'applied', order: 'applied' })
        .nodes()
        .each(function (cell, i) {
          cell.innerHTML = i + 1 + '.';
        });
    })
    .draw();
}

function isActive(value) {
  let badge;
  let text;

  switch (value) {
    case '1':
      badge = 'badge-success';
      text = 'Aktif';
      break;
    case '0':
      badge = 'badge-danger';
      text = 'Tidak Aktif';
      break;
    default:
      badge = 'badge-warning';
      text = 'error';
      break;
  }

  return `<span class="badge ${badge}">${text}</span>`;
}

function getActionButton(value) {
  return `
    <button 
      class="btn btn-info btn-sm button-edit" 
      data-id="${value}"
    >
      <i class="fa fa-edit"></i>
    </button>
    <button 
      class="btn btn-danger btn-sm button-delete" 
      data-id="${value}"
    >
      <i class="fa fa-trash"></i>
    </button>
  `;
}

function addPeserta(data, i) {
  $('#table_peserta tbody').append(`
    <tr>
      <td>
        <span class="no-urut">${i}.</span
      </td>
      <td>
        <span class="peserta-id">${data.pegawai_id}</span>
      </td>
      <td>
        <span class="peserta-nip">${data.pegawai_nip}</span>
      </td>
      <td>
        <span class="peserta-nama">${data.pegawai_nama}</span>
      </td>
      <td class="text-center">
        <button type="button" class="btn btn-sm btn-danger button-delete-peserta">
          <i class="fa fa-trash"></i>
        </button>
      </td>
    </tr>
  `);
}

function store(url, data) {
  Swal.fire({
    title: 'Apakah anda yakin untuk menyimpan?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Batal',
    confirmButtonText: 'Simpan!',
  }).then((response) => {
    if (response.value) {
      Swal.fire({
        title: 'Mohon Tunggu...',
        allowOutsideClick: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onBeforeOpen: () => {
          Swal.showLoading();
        },
        onOpen: () => {
          $.ajax({
            method: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (response) {
              if (response.message === 'success') {
                Swal.fire({
                  icon: 'success',
                  title: 'Data berhasil disimpan.',
                  showConfirmButton: true,
                }).then((response) => {
                  location.reload();
                });
              }
            },
            error: function (error, xhr) {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: error.message,
              });
            },
          });
        },
      });
    }
  });
}

function getDataById(url) {
  Swal.fire({
    title: 'Mohon Tunggu...',
    allowOutsideClick: false,
    showConfirmButton: false,
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: false,
    onBeforeOpen: () => {
      Swal.showLoading();
    },
    onOpen: () => {
      $.ajax({
        method: 'GET',
        url: url,
        success: function (response) {
          if (response.message === 'success') {
            Swal.close();

            const data = response.data;
            const member = data.member;

            $('#judul').val(data.judul);
            $('#kategori').val(data.kategori).change();
            $('#anggaran').val(data.anggaran);
            $('#mulai').val(data.tanggal_mulai);
            $('#selesai').val(data.tanggal_selesai);

            $('#table_peserta tbody').empty();

            member.forEach((element, index) => {
              let noUrut = index + 1;
              addPeserta(element, noUrut);
            });
          }
        },
        error: function (error, xhr) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: error.message,
          });
        },
      });
    },
  });
}

function update(url, data) {
  Swal.fire({
    title: 'Apakah anda yakin untuk menyimpan?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Batal',
    confirmButtonText: 'Simpan!',
  }).then((response) => {
    if (response.value) {
      Swal.fire({
        title: 'Mohon Tunggu...',
        allowOutsideClick: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onBeforeOpen: () => {
          Swal.showLoading();
        },
        onOpen: () => {
          $.ajax({
            method: 'PUT',
            url: url,
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json',
            success: function (response) {
              if (response.message === 'success') {
                Swal.fire({
                  icon: 'success',
                  title: 'Data berhasil disimpan.',
                  showConfirmButton: true,
                }).then((response) => {
                  location.reload();
                });
              }
            },
            error: function (error, xhr) {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: error.message,
              });
            },
          });
        },
      });
    }
  });
}

function destroy(url, data) {
  Swal.fire({
    title: 'Apakah anda yakin untuk menghapus?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Batal',
    confirmButtonText: 'Simpan!',
  }).then((response) => {
    if (response.value) {
      Swal.fire({
        title: 'Mohon Tunggu...',
        allowOutsideClick: false,
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onBeforeOpen: () => {
          Swal.showLoading();
        },
        onOpen: () => {
          $.ajax({
            method: 'DELETE',
            url: url,
            data: data,
            success: function (response) {
              if (response.message === 'success') {
                Swal.fire({
                  icon: 'success',
                  title: 'Data berhasil dihapus.',
                  showConfirmButton: true,
                }).then((response) => {
                  location.reload();
                });
              }
            },
            error: function (error, xhr) {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: error.message,
              });
            },
          });
        },
      });
    }
  });
}
