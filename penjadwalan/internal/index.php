<?php

include '../../core/app.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Data Peneliti</title>
  <?php include '../../core/links.php' ?>

</head>
<body>
  <input type="hidden" id="user_id" value="<?php echo $_SESSION['user_id'] ?>" readonly />
<div class="container bg-light" style="height: 100%;">
  <div class="row">
    <div class="col-sm">
      <button class="my-3 btn btn-sm btn-primary button-create">
          <i class="fas fa-plus-circle"></i>
          Tambah Baru
      </button>
    </div>
  </div>
  <div class="row">
    <div class="col-sm">
      <div class="card">
        <div class="card-header">
        <h5>Penelitian Internal</h5>
        </div>
        <div class="card-body">
          <table id="myTable" class="table table-sm table-striped table-hover" style="width:100%">
            <thead>
              <tr>
                <th>No.</th>
                <th>Judul</th>
                <th>Kategori</th>
                <th>Anggaran</th>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
                <th>Aksi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

  <!-- Modal -->
  <div id="myModal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Penjadwalan Penelitian Internal</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container">
            <form>
              <div class="form-group">
                <label for="judul">Judul Penelitian</label>
                <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Penelitian">
              </div>
              <div class="form-group">
                <label for="kategori">Kategori</label>
                <select class="form-control" id="kategori">
                  <option value="" hidden>-- Pilih kategori --</option>
                  <option value="kedokteran">Kedokteran</option>
                  <option value="keperawatan">Keperawatan</option>
                  <option value="kebidanan">Kebidanan</option>
                  <option value="penunjang">Penunjang</option>
                </select>
              </div>
              <div class="form-group">
                <label for="anggaran">Anggaran</label>
                <input type="number" class="form-control" id="anggaran" name="anggaran" placeholder="Anggaran penelitian">
              </div>
              <div class="form-group">
                <label for="mulai">Tanggal Mulai</label>
                <input type="date" class="form-control" id="mulai" name="mulai" value="<?php echo date('Y-m-d') ?>">
              </div>
              <div class="form-group">
                <label for="selesai">Tanggal Selesai</label>
                <input type="date" class="form-control" id="selesai" name="selesai" value="<?php echo date('Y-m-d') ?>">
              </div>
            </form>
            <form>
              <div class="form-group">
                <label for="peserta">Peserta</label>
                <select class="form-control" id="peserta" name="peserta"></select>
                <button type="button" class="btn btn-sm btn-success button-add-peserta my-2">
                  <i class="fa fa-plus-circle"></i>
                  Tambah
                </button>
                <div class="card">
                  <div class="card-body">
                    <table id="table_peserta" class="table table-sm table-bordered" style="width: 100%">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>ID</th>
                          <th>NIP</th>
                          <th>Nama</th>
                          <th>Hapus</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary button-modal"><i class="fas fa-save"></i> Simpan</button>
          <button type="button" class="btn btn-sm btn-danger btn-close" data-dismiss="modal"><i class="fas fa-redo"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->

<?php include '../../core/scripts.php' ?>
</body>
</html>