<?php
session_start();
if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == '') {
  echo "<script>alert('Anda belum login atau session anda habis, silakan login ulang.');
  window.location='/simrs/pendidikan_nondm/';</script>";
}
include '../connect/konek.php';
include '../head_menu.php';
include 'header.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Master Berkas</title>

  <!-- bootstrap 4 -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <!-- datatables -->
  <link rel="stylesheet" href="assets/plugins/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css">
  <!-- sweetalert2 -->
  <link rel="stylesheet" href="assets/plugins/sweetalert2/sweetalert2.min.css">
  <!-- fontawesome -->
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/fontawesome.css">
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/brands.css">
  <link rel="stylesheet" href="assets/plugins/fontawesome-5.12.1/css/solid.css">
  <!-- my-style -->
  <link rel="stylesheet" href="assets/css/my-style.css">

</head>

<body class="bg-info">
  <div class="container bg-light" style="height: 100%;">
    <div class="text-center title-page">
      <h5><b>.: Master Daftar Berkas :.</b></h5>
    </div>
    <div class="m-3">
      <button class="btn btn-sm btn-primary button-create" data-toggle="modal" data-target="#modal-berkas">
        <i class="fas fa-plus"></i>
        Tambah Daftar Berkas
      </button>
    </div>

    <!-- Table -->
    <div class="m-3">
      <table id="table-berkas" class="table table-sm table-bordered table-striped table-hover" style="width:100%;">
        <thead class="bg-light">
          <tr>
            <th scope="col" class="font-weight-bold">No.</th>
            <th scope="col" class="font-weight-bold">Nama Berkas</th>
            <th scope="col" class="font-weight-bold">Kategori Berkas</th>
            <th scope="col" class="font-weight-bold">Status Berkas</th>
            <th scope="col" class="font-weight-bold">Aksi</th>
          </tr>
        </thead>
        <tbody id="berkas-body">
        </tbody>
      </table>
    </div>
    <!-- End Table -->
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modal-berkas" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Daftar Berkas</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <form id="form-berkas">
              <div class="form-group row">
                <label for="nama_berkas" class="col-sm-3 col-form-label col-form-label-sm text-right">Nama Berkas :</label>
                <div class=" col-sm-9">
                  <input type="text" name="nama_berkas" id="nama_berkas" class="form-control form-control-sm" required="required">
                </div>
              </div>
              <div class="form-group row">
                <label for="kategori" class="col-sm-3 col-form-label col-form-label-sm text-right">Kategori :</label>
                <div class="col-sm-9">
                  <select name="kategori" id="kategori" class="form-control form-control-sm" required>
                    <option value="">-- Pilih Kategori --</option>
                    <option value="Persyaratan">Persyaratan</option>
                    <option value="Hasil">Hasil</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="status_berkas" class="col-sm-3 col-form-label col-form-label-sm text-right">Status :</label>
                <div class="col-sm-9">
                  <select name="status_berkas" id="status_berkas" class="form-control form-control-sm" required>
                    <option value="">-- Pilih Status --</option>
                    <option value="Aktif">Aktif</option>
                    <option value="Tidak Aktif">Tidak Aktif</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" name="simpan" class="btn btn-sm btn-primary button-modal"><i class="fas fa-save"></i> Simpan</button>
          <button type="button" class="btn btn-sm btn-danger btn-close" data-dismiss="modal"><i class="fas fa-redo"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->

  <!-- Optional JavaScript -->
  <script src="master-berkas.js"></script>
  <script src="assets/js/jquery-3.4.1.min.js"></script>
  <!-- <script src="assets/js/popper.min.js"></script> -->
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="assets/plugins/DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="assets/plugins/DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>

  <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
  <script src="assets/plugins/sweetalert2/sweetalert2.min.js"></script>

  <script>
    const baseUrl = 'http://localhost/api_penelitian/';

    $(document).ready(function() {
      // Get all data master berkas
      getAllBerkas(baseUrl + 'master_berkas');

      // Action tambah data
      $(".button-create").click(function() {
        $(".button-modal").addClass('button-store');
        $("#form-berkas")[0].reset();

      });

      // Action simpan data
      $("#modal-berkas").on('click', '.button-store', function(e) {
        e.preventDefault();
        let data = $("#form-berkas").serializeArray();
        store(baseUrl + 'master_berkas', data);

      });

      // Action delete data
      $("#table-berkas").on('click', '.button-delete', function() {
        fileId = $(this).data('id');

        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((response) => {
          if (response.value) {
            destroy(baseUrl + 'master_berkas/' + fileId);
          }
        });

      });

      // Action edit button
      $("#table-berkas").on('click', '.button-edit', function(e) {
        e.preventDefault();
        $(".button-modal").removeClass("button-store");
        $(".button-modal").addClass("button-update");
        userId = $(this).data('id');
        getBerkasById(baseUrl + 'master_berkas/' + userId);
      });

      // Action update button
      $("#modal-berkas").on('click', '.button-update', function(e) {
        e.preventDefault();
        let data = {
          nama: $("#nama_berkas").val(),
          kategori: $("#kategori").val(),
          status: $("#status_berkas").val()
        }

        update(baseUrl + 'master_berkas/' + userId, data);


      });


    });
  </script>
</body>

</html>