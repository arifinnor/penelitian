const apiUrl = 'http://localhost/api_penelitian/';

function getAllPembayaran(url) {
  let i = 1;

  $.ajax({
    method: 'GET',
    url: url,
    success: (response) => {
      if (response.data) {
        let pembayaran = response.data;

        pembayaran.forEach(function (value, key) {
          $("#bayar-body").append(`
            <tr>
              <td>${i++}</td>
              <td>${value.tgl_pembayaran}</td>
              <td>${value.nik}</td>
              <td>${value.nama_peneliti}</td>
              <td>${value.jenis}</td>
              <td>${value.periode}</td>
              <td>${value.jumlah_bayar}</td>
              <td>${value.verif}</td>
              <td>
                <button class="btn btn-sm btn-info button-edit" data-id="${value.id}">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger button-delete" data-id="${value.id}">
                  <i class="fas fa-trash"></i>
                </button>
              </td>
            </tr>
          `);
        });
      }
      $('#table-bayar').DataTable({
        "scrollY": "300"
      });
    }
  });
}



function getPembayaranById(url) {
  $.ajax({
    method: 'GET',
    url: url,
    success: function (response) {
      let pembayaran = response.data;

      $("#nama").empty().append(
        `<option value="${pembayaran.peneliti_id}" selected>${pembayaran.nama_peneliti}</option>`
      );
      $("#institusi").val(pembayaran.instansi);
      $("#jenis").val(pembayaran.jenis_penelitian);
      $("#nama_ruangan").val(pembayaran.ruangan);
      $("#jml_ruangan").val(pembayaran.jml_ruangan);
      $("#durasi").val(pembayaran.durasi);
      $("#jml_tagihan").val(pembayaran.jumlah_bayar);
      $("#no_kwitansi").val(pembayaran.no_kwitansi);
      $("#tgl_kwitansi").val(pembayaran.tgl_kwitansi);
      $("#keterangan").val(pembayaran.keterangan);

      // getBiaya(pembayaran.nama_penelitian);
      // getBiayaPeneliti(apiUrl + 'jenis_bayar_penelitian');

    }
  });
}



function getPeneliti(url) {
  $.ajax({
    method: 'GET',
    url: url,
    success: function (response) {
      if (response.status == '200') {
        let peneliti = response.data;
        peneliti.forEach((value, key) => {
          $("#nama").append(
            '<option data-tokens=' + value['nama_peneliti'] + ' value = ' + value['id'] + ' > ' + value['nik'] + ' - ' + value['nama_peneliti'] + '</option > '
          );
        });

      } else {
        $("#nama").append(
          '<option value =""> -- Data tidak ditemukan -- </option > '
        );
      }

    }

  });
}



// function getBiaya(name) {
//   $.ajax({
//     method: 'GET',
//     url: apiUrl + 'pembayaran/biaya/' + name,
//     success: function (response) {
//       let biaya = response.data.byr_nilai;
//       $("#nilaiBayar").val(biaya);

//     }
//   });
// }



function getBiayaPeneliti(url) {
  $.ajax({
    method: 'GET',
    url: url,
    dataType: 'JSON',
    contentType: 'application/json',
    success: function (response) {
      console.log(response);
    }
  });
}



function getPenelitiById(url) {
  $.ajax({
    method: "GET",
    url: url,
    success: function (response) {
      let result = response.data;
      // getBiaya(result['nama_penelitian']);
      $("#institusi").val(result['instansi']);
      $("#nama_ruangan").val(result['ruangan']);
      // $("#jenis").val(result['jenis_penelitian']);

    }
  });
}



function storePayment(url, data) {
  $.ajax({
    method: 'POST',
    url: url,
    data: data,
    dataType: 'json',
    success: (response) => {
      if (response.status == 201) {
        Swal.fire({
          icon: 'success',
          title: 'Your work has been saved',
          showConfirmButton: true,
        }).then((response) => {
          location.reload();
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: response.message
        });
      }
    }
  });
}



function updatePayment(url, data) {
  $.ajax({
    method: 'PUT',
    url: url,
    data: JSON.stringify(data),
    dataType: 'json',
    success: (response) => {
      if (response.status == 200) {
        Swal.fire({
          icon: 'success',
          title: 'Berhasil diupdate',
          showConfirmButton: true
        }).then(() => {
          location.reload();
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.message
        });
      }
    },
    error: function (err) {
      console.log(err);
    }
  });
}



function destroyPayment(url) {
  $.ajax({
    method: 'DELETE',
    url: url,
    success: (response) => {
      if (response.status == '200') {
        Swal.fire(
          'Deleted!',
          response.message,
          'success',
        ).then((response) => {
          location.reload();
        });
      }

    }

  });
}



function getTotalBayar() {
  $(".biaya").keyup(function () {
    var lama = $("#durasi").val();
    var nominal = $("#nilaiBayar").val();
    var ruang = $("#jml_ruangan").val();

    var totalBayar = Number(lama) * Number(nominal) * Number(ruang);

    $("#jml_tagihan").val(totalBayar);
  });
}



function getNamaPeneliti() {
  // Get Peneliti Data for Payment Purpose
  getPeneliti(apiUrl + 'pembayaran/penelitian/get_peneliti');

  // Fill the field according peneliti's data
  $("#nama").change(function () {
    let penelitiId = $(this).children("option:selected").val();

    getPenelitiById(apiUrl + 'pembayaran/penelitian/get_peneliti/' + penelitiId);
  });
}



function getJenisBayarPenelitian(url) {
  $.ajax({
    method: 'GET',
    url: url,
    success: (response) => {
      let data = response.data;
      data.forEach((el) => {
        $("#jenis").append(`
          <option value="${el.byrjns_id}" data-nilai="${el.byr_nilai}">${el.byrnama}</option>
        `);
      });
    }
  });
}