const apiUrl = '/api_penelitian/';

const baseUrl = '/api_penelitian/';

$(document).ready(function () {
  // Get All Data Pembayaran
  getAllPembayaran(baseUrl + 'pembayaran/penelitian');

  // Select2
  $('#nama').select2({
    dropdownParent: $('#modal-bayar'),
    placeholder: 'Pilih nama mahasiswa...',
    width: '100%',
    minimumInputLength: 3,
    ajax: {
      url: baseUrl + 'penelitian/tagihan/peneliti',
      dataType: 'json',
      delay: '250',
      data: function (params) {
        let query = {
          nama: params.term,
        };

        return query;
      },
      processResults: function (data) {
        let temp = data.data;

        let results = temp.map(arr => {
          return {
            id: `{"peneliti_id":"${arr.peneliti_id}","institusi":"${arr.instansi}"}`,
            text: `${arr.peneliti_nik} - ${arr.peneliti_nama}`,
          };
        });

        return {
          results: results,
        };
      },
    },
  });

  $('#modal-bayar').on('change', '#nama', function () {
    const data = JSON.parse($(this).val());
    console.log(data);
    const id = data.peneliti_id;
    const institusi = data.institusi;

    $('#institusi').val(institusi);
    getJenis(`${baseUrl}penelitian/tagihan/jenis/${id}`);
  });

  $('#modal-bayar').on('change', '#jenis', function () {
    const data = JSON.parse($(this).val());

    console.log(data);
    $('#ruangan')
      .empty()
      .append(new Option(data.ruangan, data.ruangan_id, true, true))
      .trigger('change');
  });

  // create payment
  $('.button-create').click(function (e) {
    e.preventDefault();
    // $(`#nama`).find('option[value!=""]').remove();
    $('#form-pembayaran')[0].reset();
    $('.button-modal').removeClass('button-update').addClass('button-simpan');
    $('.button-simpan').removeAttr('data-id');

    $('#modal-bayar').find('.modal-title').text('Buat Tagihan');

    // Get data jenis pembayaran
    // $("#jenis").empty().append('<option value="">-- Pilih jenis pembayaran --</option>');
    // getJenisBayarPenelitian(baseUrl + "pembayaran/jenis_bayar_penelitian");
    // getBiayaPeneliti(baseUrl + 'pembayaran/jenis_bayar_penelitian');
  });

  // Store payment
  $('#modal-bayar').on('click', '.button-simpan', function (e) {
    e.preventDefault();
    let data = $('#form-pembayaran').serializeArray();

    storePayment(baseUrl + 'pembayaran/penelitian', data);
  });

  // close modal
  $('#modal-bayar').on('click', '.btn-close', function (e) {
    // console.log('batal');
    e.preventDefault();
    // ("#modal-bayar").modal('hide');
    $('#form-pembayaran')[0].reset();
  });

  // Edit payment
  $('#table-bayar').on('click', '.button-edit', function (e) {
    e.preventDefault();
    let id = $(this).data('id');
    // console.log(id);
    // $("#nama").find('option').remove();
    $('.button-modal').removeClass('button-simpan').addClass('button-update');
    $('.button-update').attr('data-id', id);
    $('#modal-bayar').modal('show');

    getPembayaranById(baseUrl + 'pembayaran/penelitian/' + id);
  });

  // Update payment
  $('#modal-bayar').on('click', '.button-update', function (e) {
    e.preventDefault();
    let id = $(this).data('id');

    let data = {
      nama: $('#nama').val(),
      jml_ruangan: $('#jml_ruangan').val(),
      durasi: $('#durasi').val(),
      jml_tagihan: $('#jml_tagihan').val(),
      no_kwitansi: $('#no_kwitansi').val(),
      tgl_kwitansi: $('#tgl_kwitansi').val(),
      keterangan: $('#keterangan').val(),
    };

    updatePayment(baseUrl + 'pembayaran/penelitian/' + id, data);
  });

  // Delete payment
  $('#table-bayar').on('click', '.button-delete', function () {
    let id = $(this).data('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then(response => {
      if (response.value) {
        destroyPayment(baseUrl + 'pembayaran/penelitian/' + id);
      }
    });
  });

  // Count total payment
  // getTotalBayar();

  //
  $('#modal-bayar').on('change', '#jenis', function () {
    // let nilaiBayar = $(this).data("nilai");
    // let id = this.value;
    // console.log(nilaiBayar);
    // $("#nilaiBayar").val(nilaiBayar);
  });
});

function getJenis(url) {
  $.ajax({
    method: 'GET',
    url: url,
    dataType: 'json',
    success: response => {
      if (response.status == 200) {
        let data = response.data;

        data.forEach(element => {
          $('#jenis').append(`
            <option value='{
                "jadwal_id": "${element.jadwal_id}",
                "jenis": "${element.jenis_penelitian}",
                "ruangan_id": "${element.ruangan_id}",
                "ruangan": "${element.ruangan}",
                "tgl_mulai": "${element.tgl_mulai}",
                "tgl_selesai": "${element.tgl_selesai}"
                }'
            >
                ${setJenis(element.jenis_penelitian)}
            </option>
          `);
        });
      }
    },
  });
}

function setJenis(val) {
  switch (val) {
    case '1':
      return 'Data Awal & Residensi';
    case '2':
      return 'Penelitian';
    case '3':
      return 'Penelitian Tanpa Surat Kelayakan Etik';
    case '4':
      return 'Penelitian Dengan Surat Kelayakan Etik';
    case '5':
      return 'Penerbitan Surat Kelayakan Etik Tanpa Penelitian';

    default:
      return 'Undefined';
  }
}
